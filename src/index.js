import "@picocss/pico";
import "./asset/css/style.css";
require("./component/notifications/index");
require("./component/messages/index");
require("./component/auth/index");
require("./component/alpine/index");
