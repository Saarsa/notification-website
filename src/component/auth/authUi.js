import * as firebaseui from 'firebaseui'
import 'firebaseui/dist/firebaseui.css'
import {
    EmailAuthProvider,
    GoogleAuthProvider,
    FacebookAuthProvider,
    TwitterAuthProvider,
    GithubAuthProvider,
    PhoneAuthProvider
} from "firebase/auth";
import { auth } from '../firebaseApp';

const uiConfig = {
    signInSuccessUrl: 'https://localhost:8081/',
    signInOptions: [
        // EmailAuthProvider.PROVIDER_ID,
        // GoogleAuthProvider.PROVIDER_ID,
        // FacebookAuthProvider.PROVIDER_ID,
        // TwitterAuthProvider.PROVIDER_ID,
        // GithubAuthProvider.PROVIDER_ID,
        // PhoneAuthProvider.PROVIDER_ID,
        // firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
    ],
    tosUrl: 'https://localhost:8081/TOS',
    privacyPolicyUrl: function () {
        window.location.assign('https://localhost:8081/PP');
    }
};

let ui = new firebaseui.auth.AuthUI(auth);
ui.start('#firebaseui-auth-container', uiConfig);
