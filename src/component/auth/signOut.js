import { signOut } from "firebase/auth";
import { auth } from "../firebaseApp";
document.addEventListener("alpine:user:init", ({ detail: AlpineUser }) => {
    document.addEventListener("auth:listener:logOut", () => { AlpineUser.login = false; });
    Alpine.magic("signOut", (el) => { signOut(auth); });
});