import { auth } from "../firebaseApp";
import { signInWithRedirect } from "firebase/auth";
import { provider } from "./Provider";
document.addEventListener("alpine:user:init", ({ detail: AlpineUser }) => {
    document.addEventListener("auth:listener:login", (e) => { AlpineUser.login = true; AlpineUser.userUid = e.detail.uid; });
    Alpine.magic("login", el => { signInWithRedirect(auth, provider); });
});