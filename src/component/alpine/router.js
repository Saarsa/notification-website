function router() {
    return {
        main(context) {
            document.querySelector('#app').innerHTML = `<h1>Home</h1>`;
        },
        checkName(context) {
            // if the name is "home" go to the home page.
            if (context.params.name.toLowerCase() == 'home') {
                // redirecting is done by returning the context.redirect method.
                return context.redirect('/');
            }
        },
        hello(context) {
            document.querySelector(
                '#app'
            ).innerHTML = `<h1>Hello, ${context.params.name}</h1>`;
        },
        notfound(context) {
            document.querySelector('#app').innerHTML = `<h1>Not Found</h1>`;
        },
    };
}