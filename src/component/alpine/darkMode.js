document.addEventListener('alpine:init', () => {
    let darkMode = {
        dark: Alpine.$persist(true),
        get isDark() { return this.dark ? "dark" : "light"; },
    }
    darkMode = Alpine.reactive(darkMode)
    Alpine.data("darkMode", () => darkMode);
    Alpine.effect(() => { document.documentElement.setAttribute("data-theme", darkMode.isDark); });
});