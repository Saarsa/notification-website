document.addEventListener("alpine:init", () => {
  let dialog = {
    init() {
      document.addEventListener("notification:click", () => {
        this.toggle();
      });
    },
    status: false,
    isOpen() {
      return this.status;
    },

    toggle() {
      if (!Alpine.user.login) {
        alert(JSON.stringify(window.AlpineI18n.t("dialog.login.error.login")));
        return;
      }
      if (Alpine.user.login && !this.status) {
        this.status = true;
        return;
      }

      if (this.status) {
        this.status = false;
      }
    },
  };
  dialog = Alpine.reactive(dialog);
  Alpine.data("dialog", () => dialog);
});
