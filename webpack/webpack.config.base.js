const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackSkipAssetsPlugin =
    require("html-webpack-skip-assets-plugin").HtmlWebpackSkipAssetsPlugin;
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')



module.exports = {
    entry: {
        bundel: path.resolve(__dirname, "../src/index.js"),
        "firebase-messaging-sw": path.resolve(__dirname, "../src/firebase-messaging-sw.js"),
        // sw: path.resolve(__dirname, "../src/service-worker.js"),
    },
    output: {
        path: path.resolve(__dirname, "../public"),
        filename: (pathData) => {
            return pathData.chunk.name != "firebase-messaging-sw" && pathData.chunk.name != "sw" ? "[name][contenthash].js" : "[name].js";
        },
        clean: true,
        assetModuleFilename: "[name][ext]",
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "notification website",
            filename: "index.html",
            template: "src/template.html",
            excludeAssets: [/firebase-messaging-sw.js/, /sw.js/],
        }),
        new HtmlWebpackSkipAssetsPlugin(),
        new CopyPlugin({
            patterns: [
                // language
                {
                    from: path.resolve(__dirname, "../src/asset/language/language.lang"),
                    to: "language",
                },
                {
                    from: path.resolve(__dirname, "../src/asset/language/language.json"),
                    to: "language",
                },
            ],
        }),
        // new FaviconsWebpackPlugin({
        //     logo: path.resolve(__dirname, "../src/asset/images/envelope-512x512.png"),
        //     prefix: 'assets/images/',
        //     favicons: {
        //         start_url: "/index.html",
        //         display: "fullscreen",
        //         scope: "/",
        //         developerName: "Saar Sabo",
        //         preferRelatedApplications: true,
        //         version: "1.0",
        //     }
        // }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"],
                    },
                },
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: "asset/resource",
            },
            {
                test: /\.less$/i,
                use: ["style-loader", "css-loader", "less-loader"],
            },
        ],
    },
}