const path = require("path");
const Dotenv = require("dotenv-webpack");
const baseConfig = require("./webpack.config.base");
const { merge } = require('webpack-merge');
module.exports = merge(baseConfig, {
    mode: "development",
    devServer: {
        static: {
            directory: path.resolve(__dirname, "../public"),
        },
        allowedHosts: ["laptop"],
        port: 8081,
        open: true,
        hot: true,
        compress: true,
        historyApiFallback: true,
    },
    devtool: "inline-source-map",
    plugins: [
        new Dotenv({
            path: path.resolve(__dirname, "../src/.env"),
        }),
    ]
})