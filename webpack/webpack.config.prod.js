const { DefinePlugin } = require('webpack');
const baseConfig = require("./webpack.config.base");
const { merge } = require('webpack-merge');
console.log({
    APIKEY: JSON.stringify(process.env.APIKEY),
    AUTHDOMAIN: JSON.stringify(process.env.AUTHDOMAIN),
    PROJECTID: JSON.stringify(process.env.PROJECTID),
    STORAGEBUCKET: JSON.stringify(process.env.STORAGEBUCKET),
    MESSAGINGSENDERID: JSON.stringify(process.env.MESSAGINGSENDERID),
    APPID: JSON.stringify(process.env.APPID),
    LOCALHOST: JSON.stringify(process.env.LOCALHOST),
    APIURL: JSON.stringify(process.env.APIURL),
    VAPIDKEY: JSON.stringify(process.env.VAPIDKEY),
    DEBUG: JSON.stringify(process.env.DEBUG),
    MEASUREMENTID: JSON.stringify(process.env.MEASUREMENTID)
});

module.exports = merge(baseConfig, {
    mode: "production",
    devtool: "source-map",
    plugins: [
        new DefinePlugin({
            "process.env.APIKEY": JSON.stringify(process.env.APIKEY),
            "process.env.AUTHDOMAIN": JSON.stringify(process.env.AUTHDOMAIN),
            "process.env.PROJECTID": JSON.stringify(process.env.PROJECTID),
            "process.env.STORAGEBUCKET": JSON.stringify(process.env.STORAGEBUCKET),
            "process.env.MESSAGINGSENDERID": JSON.stringify(process.env.MESSAGINGSENDERID),
            "process.env.APPID": JSON.stringify(process.env.APPID),
            "process.env.LOCALHOST": JSON.stringify(process.env.LOCALHOST),
            "process.env.APIURL": JSON.stringify(process.env.APIURL),
            "process.env.VAPIDKEY": JSON.stringify(process.env.VAPIDKEY),
            "process.env.MEASUREMENTID": JSON.stringify(process.env.MEASUREMENTID),
            "process.env.DEBUG": JSON.stringify(process.env.DEBUG)
        }),
    ]
})









