#!/usr/bin/env node
var fs = require("fs");
const path = require("path");
const prompts = require("prompts");
const argv = require('yargs').argv

const request = async (project_name) => {
  try { fs.lstatSync(path.resolve(__dirname, "src/__/auth/")).isDirectory() }
  catch (error) { fs.mkdir(path.resolve(__dirname, "src/__/auth/"), { recursive: true }, error => { }) }
  finally {
    console.log(`Downloading file for the project name: ${project_name}`);
    for (file of ["handler", "handler.js", "experiments.js", "iframe", "iframe.js"]) {
      const respone = await fetch(`https://${project_name}.firebaseapp.com/__/auth/${file}`);
      const text = await respone.text();
      await fs.writeFile(`src/__/auth/${file}`, text, error => { if (error) throw error; console.log(`src/__/auth/${file} Saved!`); });
    };
  };
  return true;
};

const get_projects_name = (file) => JSON.parse(fs.readFileSync(file, 'utf8')).projects;

const get_file_name = file => {
  if (file.indexOf(".firebaserc") != -1 && fs.lstatSync(file).isFile()) return file;
  if (file.indexOf(".firebaserc") == -1 && fs.lstatSync(path.resolve(file, ".firebaserc")).isFile()) return path.resolve(file, ".firebaserc");
}

const create_choices_by_key = data => {
  const file = get_file_name(data);
  const projects = get_projects_name(file);
  const test = []
  for (const [key, value] of Object.entries(projects))
    test.push({ title: key, value: value })
  test.push({ title: "please let my choose it by name", value: file })
  return test
}

const create_choices_by_name = data => {
  const file = get_file_name(data);
  const projects = get_projects_name(file);
  const test = []
  for (const [key, value] of Object.entries(projects))
    test.push({ title: value, value: value })
  return test
}

const questions = [
  { type: "select", name: "step_1", message: "How do you want to provide the project name?", choices: [{ title: "file", value: "file" }, { title: "name", value: "name" }] },
  { type: prev => prev == "file" ? "text" : null, name: "step_2", message: "What is path to the file?", hidden: "file" },
  { type: prev => prev == "name" ? "text" : null, name: "step_2", message: "What is the name of the project?", hidden: "name" },
  { type: "select", name: "step_3", message: "Please choose the correct project you want", choices: create_choices_by_key, hidden: "step_1" },
  { type: "select", name: "step_3", message: "Please choose the correct project you want", choices: create_choices_by_name, hidden: "go" }
]

const onSubmit = async (prompt, answer) => {
  console.log(prompt.hidden);
  if (prompt.hidden == "name") return await request(answer);
  if (prompt.hidden == "file") {
    const file = get_file_name(answer)
    const projects = get_projects_name(file)
    if ((Object.keys(projects)).length == 1) return await request(projects[Object.keys(projects)[0]]);
  }
  if (prompt.name == "step_3") {
    if (prompt.hidden == "go") return await request(answer);
    if (prompt.hidden == "step_1" && prompt.choices.map(x => x.value).indexOf(answer) != 3) return await request(answer);
  }
  return false;
}

(async () => {
  if (argv.name) return request(argv.name);
  console.log("Hello user here I will help you");
  await prompts(questions, { onSubmit })
})();
