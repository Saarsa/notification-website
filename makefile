.DEFAULT_GOAL := help

help:  ## 💬 This help message
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

get_java_20: ## download java 20 and install  
	@curl -L  https://download.oracle.com/java/20/archive/jdk-20.0.1_linux-aarch64_bin.tar.gz > jdk-20.tar.gz
	@sudo tar xvf jdk-20.tar.gz -C /usr/bin
    # @echo 'export PATH="/usr/bin/jdk-20.0.1/bin/:$PATH"'>> .profile
	@export PATH="/usr/bin/jdk-20.0.1/bin/:$PATH"
	@rm ./jdk-20.tar.gz

get_java_19: ## download java 19 and install  
	@curl -L  https://download.oracle.com/java/19/archive/jdk-19.0.2_linux-aarch64_bin.tar.gz > jdk-19.tar.gz
	@sudo tar xvf jdk-19.tar.gz -C /usr/bin
    # @echo 'export PATH="/usr/bin/jdk-19.0.2/bin/:$PATH"'>> .profile
	@export PATH="/usr/bin/jdk-19.0.2/bin/:$PATH"
	@rm ./jdk-19.tar.gz

start_emulators: ## Start the emulators
	firebase emulators:start

init_firebase: ## Init firebase
	firebase init
