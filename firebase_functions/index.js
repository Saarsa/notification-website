// options
const { setGlobalOptions } = require("firebase-functions/v2");
// admin
const { initializeApp } = require("firebase-admin/app");
const { getFirestore } = require("firebase-admin/firestore");
const { getMessaging } = require("firebase-admin/messaging");
// functions
const {
    onRequest,
    CallableRequest,
    onCall,
    HttpsError,
} = require("firebase-functions/v2/https");
const { onDocumentCreated } = require("firebase-functions/v2/firestore");
const { auth, logger } = require("firebase-functions");
// firestore
const { FieldValue } = require("@google-cloud/firestore");
// utils
const _generateApiKey = require("generate-api-key").default;
const uuidv4 = require("uuid").v4;
const express = require("express");
const cors = require("cors");
// codes
initializeApp();
setGlobalOptions({ enforceAppCheck: false, region: "us-central1" });
const firestore = getFirestore();
const messaging = getMessaging()
exports.generateApiKey = auth.user().onCreate(async (user) => {
    const apiKey = _generateApiKey({ method: "string", length: 27 });
    return await firestore
        .doc(`users/${user.uid}`)
        .set({ apiKey: apiKey, devices: {} });
});

exports.deleteApiKey = auth.user().onDelete(async (user) => {
    const doc = firestore.doc(`users/${user.uid}`);
    return await firestore.recursiveDelete(doc);
});

exports.deleteCollection = onCall(async ({ app, data }) => {
    // if (app == undefined)
    //     throw new HttpsError(
    //         "failed-precondition",
    //         "The function must be called from an App Check verified app."
    //     );
    const collection = firestore.collection(`users/${data.userUid}/messags`);
    return await firestore.recursiveDelete(collection);
});

exports.sendNotifications = onDocumentCreated(
    "users/{userId}/messags/{messagsId}",
    async ({ data }) => {
        const payload = {
            notification: {
                title: data.data().title,
                body: data.data().body,
            },
        };
        const userDoc = await data.ref.parent.parent.get();
        const devices = Object.values(userDoc.get("devices"));
        devices.forEach(async (device) => {
            payload["token"] = device;
            await messaging.send(payload);
        });
    }
);

const http = express();
http.use(cors({ origin: true }));

http.post("/api/createMessage", async (req, res) => {
    const apiKey = req.headers.apikey;
    if (!apiKey) res.status(401).send(`not provided api key please check if you provided the api key like it need`);
    if (!req.body.notification) return res.status(422).send("missing notification");
    const doc = firestore.collection("users").where("apiKey", "==", apiKey);
    const userDoc = (await doc.get()).docs[0];
    if (!userDoc) res.status(403).send(`Check the api key ${apiKey}`);
    userDoc.ref.collection("messags").doc(uuidv4()).set({ ...req.body.notification, EpochTime: FieldValue.serverTimestamp() });
    res.status(200).send(`Set notification to ${apiKey}`);
});

http.get("/api", async (req, res) => {
    res.status(200).send(`Set notification to`);
});

// Expose Express API as a single Cloud Function:
exports.api = onRequest(http);
